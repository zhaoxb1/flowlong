package com.flowlong.bpm.engine.core.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.flowlong.bpm.engine.entity.HisTaskActor;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

public interface HisTaskActorMapper extends CommonMapper<HisTaskActor> {

}
