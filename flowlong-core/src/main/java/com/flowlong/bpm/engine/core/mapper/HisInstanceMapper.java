package com.flowlong.bpm.engine.core.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.flowlong.bpm.engine.entity.HisInstance;

public interface HisInstanceMapper extends BaseMapper<HisInstance> {

}
