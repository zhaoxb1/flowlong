package com.flowlong.bpm.engine.core.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.flowlong.bpm.engine.entity.CCInstance;

public interface CCInstanceMapper extends BaseMapper<CCInstance> {

}
